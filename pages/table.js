import React from 'react'
import router from 'next/router'
import { useState } from "react";
import { Container, InputGroup, FormControl, Col, Row, Button, FloatingLabel, Form, Pagination } from 'react-bootstrap'
import Tables from '../Component/Table'

function Table({userData, page, gender}) {
  const [optionState, setSelOption] = useState({});
  const paginate = [];

  for (var i = 1; i <= 3; i++) {
    if(page == 1){
      if(i == 3){
        paginate.push(<Pagination.Ellipsis />);
      }else{
        paginate.push(<Pagination.Item onClick={() => router.push(`/table?page=${i}`)}>{i}</Pagination.Item>);
      }
    }else{
      if(i == 2){
        paginate.push(<Pagination.Item onClick={() => router.push(`/table?page=${i}`)}>{page}</Pagination.Item>);
      }else{
        paginate.push(<Pagination.Ellipsis />);
      }
    }
  }

  const handleReset = () => {
    setSelOption("");   
    router.push(`/table`)
  }

  const HandelChange = (obj) => {
    setSelOption(obj);
    router.push(`/table?gender=${obj.target.value}`)
  };
  
  console.log(userData);

  return (
    <div>
      <Container className="mt-5">
        {/* <Col className='mb-4'> */}
          <Col md={12} className="d-flex flex-row mb-4"> 
            <div className='d-flex flex-row me-4'>
                <InputGroup className="my-auto">
                  <FloatingLabel controlId="floatingSelect" label="Search">
                    <FormControl
                      placeholder="Recipient's username"
                      aria-label="Recipient's username"
                      aria-describedby="basic-addon2"
                    />
                  </FloatingLabel>
                </InputGroup>
                <div className='d-flex'>
                  <Button variant="primary" id="button-addon2">
                    Button
                  </Button>
                </div>
            </div>
            <div className='d-flex flex-row'>
              <FloatingLabel controlId="floatingSelect" label="Gender">
                <Form.Select aria-label="Floating label select example" value={optionState} onChange={(option) => HandelChange(option)}>
                  <option value="" disabled selected>Select Gender</option>
                  <option value="male">Male</option>
                  <option value="female">Female</option>
                </Form.Select>
              </FloatingLabel>
              <Button variant="outline-secondary" className="ms-2" id="button-addon2" onClick={() => handleReset()}>
                  Reset Filter
              </Button>
            </div>
          </Col>
        <Tables userData={userData}></Tables>
        <Col className='d-flex justify-content-end'>
          <Pagination>
            <Pagination.Prev disabled={page <= 1} onClick={() => router.push(`/table?page=${page - 1}`)}/>
            {paginate}
            <Pagination.Next onClick={() => router.push(`/table?page=${page + 1}`)}/>
          </Pagination>
        </Col>
      </Container>
    </div>
  )
}

export async function getServerSideProps({query: {page=1, gender=""},}) {
  let apiRef = ""

  if(!gender){
    apiRef = `https://randomuser.me/api/?page=${page}&results=10&seed=1a104d26031f49fe`
  }else{
    apiRef = `https://randomuser.me/api/?page=${page}&results=10&gender=${gender}`
  }
  const res = await fetch(apiRef)
  const data = await res.json()

  return {
    props: { 
      userData: data,
      page: +page
     },
  }
}

export default Table
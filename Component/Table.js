import Head from 'next/head'
import Image from 'next/image'
import Table from 'react-bootstrap/Table'

export default function Tables({userData}) {
  return (
    <div>
        <Table striped bordered hover>
            <thead>
                <tr>
                <th>Username</th>
                <th>Name</th>
                <th>Email</th>
                <th>Gender</th>
                <th>Registered Date</th>
                </tr>
            </thead>
            <tbody>
                {userData.results.map((user, index) => (
                    <tr key={index+1}>
                        <td>{user.login.username}</td>
                        <td>{user.name.first + " " + user.name.last}</td>
                        <td>{user.email}</td>
                        <td>{user.gender}</td>
                        <td>{user.registered.date}</td>
                    </tr>
                ))}
            </tbody>
        </Table>
    </div>
  )
}
